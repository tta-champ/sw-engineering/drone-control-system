# 1. Use PostgreSQL Database

Date: 2022-01-07

## Status

Accepted

## Context

RDB와 NoSQL 중 관리 목적상 RDB가 적절하다고 봄.
PostgreSQL과 MySQL 중 Cloud 환경과 지원 범위가 더 넓은 PostgreSQL가 적합하다고 봄.


## Decision

DB를 PostgreSQL로 결정한다

## Consequences

PostgreSQL 설치는 IaC로 되도록 구현한다.
ORM solution은 다음 ADR에서 논의한다.

