
# 2. Use NestJS as API Server

Date: 2022-02-21

## Status

Superceded by [3. fast for python developer](0003-fast-for-python-developer.md)

## Context

Frontend에 API를 제공할 backend framework를 설정할 후보군으로
Python 기반의 FastAPI,
NodeJS 기반의 NestJS, Express, Koa, Hapi, NextJS, NuxtJS
중 Typescript기반의 경량, 사용자가 많고, Update가 많은 NestJS가 가장 유리하다고 판단한다.

## Decision

NestJS를 API Server Framework로 사용한다

## Consequences

NestJS 기반 scafolding 구성후 PostgreSQL과 ORM연동 검증 후, Dockerize한다.
