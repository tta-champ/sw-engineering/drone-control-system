# 3. fast for python developer

Date: 2022-02-23

## Status

Accepted

Supercedes [2. Use NestJS as API Server](0002-use-nestjs-api-server.md)

## Context

Python에 익숙한 AI 개발자와, 
Node기반 NestJS는 새로 배워야하는 진입 장벽

## Decision

API Server는 Python 기반 FAST-API를 Framework로 한다.

## Consequences

FastAPI로 OpenAPI 명세서 기반 설계
ORM은 Python에 많이 쓰는 sqlalchemy로 검토한다.

