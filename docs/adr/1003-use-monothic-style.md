# 1003. Use Monothic style

Date: 2022-04-26

## Status

Accepted

Supercedes [1002. Use MSA](1002-use-msa-style.md)

## Context

MSA 적용하려고 보니, 너무 난이도 높아 어려워서 포기

## Decision

그냥 monithic 로 결정


## Consequences

기 구현한 msa 관련 코드는 revert 시킨다.
